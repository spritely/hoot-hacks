window.addEventListener("load", function() {
	Scheme.load_main("websocket-echo-client.wasm", {}, {
		element: {
			addEventListener(elem, name, f) { elem.addEventListener(name, f); }
		},
		MessageEvent: {
			data(message_event) { return message_event.data; }
		},
		WebSocket: {
			close(socket) { socket.close(); },
			new(url) { return new WebSocket(url); },
			send(socket, data) { socket.send(data); }
		}
	});
});
