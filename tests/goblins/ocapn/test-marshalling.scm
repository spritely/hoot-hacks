(use-modules (guile-compat srfi srfi-64)
             (srfi srfi-9)
             (goblins abstract-types)
             (goblins ocapn marshalling)
             (goblins contrib syrup)
             (ice-9 match)
             ((scheme base) #:select (string->utf8))
             (test-runner))

(test-begin "marshalling")

(define-record-type <animal>
  (make-animal name noise)
  animal?
  (name animal-name)
  (noise animal-noise))

(define serialize-animal
  (match-lambda
    (($ <animal> name noise)
     (list name noise))))

(define-values (marshall::animal unmarshall::animal)
  (make-marshallers 'animal animal?
                    serialize-animal make-animal))

(define-record-type <fruit>
  (make-fruit name color)
  fruit?
  (name fruit-name)
  (color fruit-color))

(define serialize-fruit
  (match-lambda
    (($ <fruit> name color)
     (list name color))))

(define-values (marshall::fruit unmarshall::fruit)
  (make-marshallers 'fruit fruit?
                    serialize-fruit make-fruit))

(define marshallers
  (list marshall::animal
        marshall::fruit))
(define unmarshallers
  (list unmarshall::animal
        unmarshall::fruit))

(define cat (make-animal "Cat" 'meow))
(define banana (make-fruit "Banana" 'yellow))

(define friends
  `((cat-friend ,cat)
    (banana-friend ,banana)))
(define marshalled-friends
  (string->utf8
   "[[10'cat-friend<6'animal3\"Cat4'meow>][13'banana-friend<5'fruit6\"Banana6'yellow>]]"))

(test-assert
    "Check that the can-marshall function works on its record type"
  ((car marshall::animal) cat))

(test-assert
    "Check that the can-marshall function returns false when given incorrect record type"
  (not ((car marshall::animal) banana)))

(define sticky-cat ((cdr marshall::animal) cat))
(test-assert
    "Check that marshalled cat returns syrup record"
  (tagged? sticky-cat))

(test-equal "Check that syrup-encode will marshall with marshallers correctly"
  marshalled-friends
  (syrup-encode friends
                #:marshallers marshallers))

(test-equal "Check that syrup-encode will unmarshall with unmarshallers correctly"
  friends
  (syrup-decode marshalled-friends
                #:unmarshallers unmarshallers))

(test-assert
    "Check the can-unmarshall function works for the correct label"
  ((car unmarshall::animal) (tagged-label sticky-cat)))

(define sticky-banana ((cdr marshall::fruit) banana))
(test-assert
    "Check that can-unmarshall returns false for the wrong label"
  (not ((car unmarshall::animal) (tagged-label sticky-banana))))

(test-equal "Check that unmarshalling returns correct data"
  cat
  (apply (cdr unmarshall::animal) (tagged-data sticky-cat)))

(test-end "marshalling")
