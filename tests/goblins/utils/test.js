// -*- js-indent-level: 4 -*-

let bindings = {
    typedArray: {
        makeUint8Array: (length) => new Uint8Array(length),
        Uint8ArrayLength: (array) => array.length,
        Uint8ArrayRef: (array, index) => array[index],
        Uint8ArraySet: (array, index, value) => array[index] = value
    },
    crypto: {
        digest: (algorithm, data) => globalThis.crypto.subtle
            .digest(algorithm, data).then((arrBuf) => new Uint8Array(arrBuf)),
        getRandomValues: (array) => globalThis.crypto.getRandomValues(array),
        generateEd25519KeyPair: () => globalThis.crypto.subtle.generateKey(
            { name: "Ed25519" },
            true,
            ["sign", "verify"]
        ),
        keyPairPrivateKey: (keyPair) => keyPair.privateKey,
        keyPairPublicKey: (keyPair) => keyPair.publicKey,
        exportKey: (key) => globalThis.crypto.subtle.exportKey("raw", key)
            .then((arrBuf) => new Uint8Array(arrBuf)),
        importPublicKey: (key) => globalThis.crypto.subtle.importKey(
            "raw",
            key,
            { name: "Ed25519" },
            true,
            ["verify"]
        ),
        signEd25519: (data, privateKey) => globalThis.crypto.subtle.sign(
            { name: "Ed25519" },
            privateKey,
            data
        ).then((arrBuf) => new Uint8Array(arrBuf)),
        verifyEd25519: (signature, data, publicKey) => globalThis.crypto.subtle
            .verify(
                { name: "Ed25519" },
                publicKey,
                signature,
                data
            )
    }};

if (typeof exports === 'undefined') {
    window.addEventListener("load", async () => {
        const [proc] = await Scheme.load_main("test.wasm", {
            user_imports: bindings
        });
        proc.call_async();
    });
} else {
    exports.user_imports = bindings;
}
