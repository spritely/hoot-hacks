(use-modules (goblins utils crypto)
             (ice-9 match)
             (fibers promises)
             (guile-compat srfi srfi-64)
             (rnrs bytevectors)
             (test-runner))

(define (main)
  (test-begin "test-crypto")

  (define test-bv
    (string->utf8
     "Centralized social media is harmful to society. We are building a gatekeeper-free decentralized system. Our mission is “social media done right”, to put people in control of their own identity and build the technology that would enable a shift to collaborative and intentional security models prioritizing active consent. To accomplish this, we will build a new architecture for the internet: removing the necessity of client-server architecture, replacing it with a participatory peer-centric model."))

  (define bv (strong-random-bytes))

  (define key-pair (generate-key-pair))
  (define private-key (key-pair->private-key key-pair))
  (define public-key (key-pair->public-key key-pair))

  (test-assert "public key extracted to correct format"
    (match public-key
      (`(public-key (ecc (curve Ed25519)
                         (flags eddsa)
                         (q ,(? bytevector?))))
       #t)
      (_ #f)))

  (define signature (sign test-bv private-key))

  (test-assert "signature has correct format"
    (signature-sexp? signature))

  (test-assert "signature verifies"
    (verify (captp-signature->crypto-signature signature)
            test-bv
            (captp-public-key->crypto-public-key public-key)))

  (test-end))

(lambda (resolved rejected)
  (call-with-async-result
   resolved rejected
   main))
