(use-modules (fibers)
             (fibers channels)
             ((fibers promises)
              #:select (call-with-async-result))
             (ice-9 match)
             (ice-9 q)
             (guile-compat srfi srfi-64)
             (goblins inbox)
             (test-runner))

(define (main)
  (test-begin "test-inbox")

  ;; Testing that the delivery agent queues and de-queues messages
  (define-values (enq-ch deq-ch stop?)
    (spawn-delivery-agent))
  (put-message enq-ch 'foo)
  (put-message enq-ch 'bar)
  (put-message enq-ch 'baz)
  (test-equal 'foo (get-message deq-ch))
  (test-equal 'bar (get-message deq-ch))
  (test-equal 'baz (get-message deq-ch))

  (test-end "test-inbox"))

(lambda (resolved rejected)
  (call-with-async-result
   resolved rejected
   main))
