;;; Copyright 2019-2021 Christine Lemmer-Webber
;;; Copyright 2024 Juliana Sims
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(import (guile-compat srfi srfi-64)
        (only (hoot syntax) define*)
        (pre-goblins goblins-stage1)
        (test-runner)
        (scheme base)
        (scheme case-lambda)
        (scheme write))

(test-begin "test-goblins-stage1")

(define am (make-whactormap))

(define (^greeter _bcom my-name)
  (lambda (your-name)
    (string-append "Hello " your-name ", my name is " my-name "!")))

(define alice
  (actormap-direct-run!
   am
   (lambda ()
     (spawn ^greeter "Alice"))))

(test-equal "Hello Bob, my name is Alice!"
  (actormap-direct-run!
   am
   (lambda ()
     ($ alice "Bob"))))

(define (^gregarious _bcom my-name)
  (lambda (talk-to)
    (string-append "I heard back: "
                   ($ talk-to my-name))))

(define greg
  (actormap-direct-run!
   am
   (lambda ()
     (spawn ^gregarious "Greg"))))

;; Actors which call other actors
(test-equal "I heard back: Hello Greg, my name is Alice!"
  (actormap-direct-run!
   am
   (lambda ()
     ($ greg alice))))

;; Actor updates: update and return value separately
(define* (^cell bcom #:optional [val #f])
  (case-lambda
    [() val]
    [(new-val) (bcom (^cell bcom new-val))]))

(actormap-direct-run!
 am
 (lambda ()
   (define cell (spawn ^cell))
   (test-equal ($ cell) #f)             ; initial val
   ($ cell 'foo)                        ; update (no return value)
   (test-equal ($ cell) 'foo)))         ; new val

;; Actor updates: update and return value at same time
(define* (^counter bcom #:optional [n 0])
  (lambda ()
    (bcom (^counter bcom (+ n 1)) n)))

(actormap-direct-run!
 am
 (lambda ()
   (define ctr (spawn ^counter))
   (test-equal 0 ($ ctr))
   (test-equal 1 ($ ctr))
   (test-equal 2 ($ ctr))
   (test-equal 3 ($ ctr))))

(test-end "test-goblins-stage1")
#t
