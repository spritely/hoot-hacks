;;; Copyright 2024 Juliana Sims
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; Test for pre-goblins stage0.
;;;
;;; Code:

(import (guile-compat srfi srfi-64)
        (pre-goblins goblins-stage0)
        (test-runner)
        (scheme base)
        (scheme write))

(test-begin "test-goblins-stage0")

(define am (make-whactormap))

(define (^greeter _bcom my-name)
  (lambda (your-name)
    (string-append "Hello " your-name ", my name is " my-name "!")))

(define alice (actormap-spawn! am ^greeter "Alice"))

(test-equal "Hello Bob, my name is Alice!"
  (actormap-peek am alice "Bob"))

(test-end "test-goblins-stage0")
#t
