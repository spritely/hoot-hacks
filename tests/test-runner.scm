;;; Copyright 2024 Juliana Sims
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; SRFI 64 test runner for pre-goblins.
;;;
;;; Code:

(define-module (test-runner)
  #:use-module (guile-compat srfi srfi-64))

;; Based on the example test runner code from SRFI 64
;; https://srfi.schemers.org/srfi-64/srfi-64.html
(test-runner-factory
 (lambda ()
   (define runner (test-runner-null))
   (test-runner-on-final! runner
     (lambda (runner)
       (let* ((pass (test-runner-pass-count runner))
              (fail (test-runner-fail-count runner))
              (xfail (test-runner-xfail-count runner))
              (xpass (test-runner-xpass-count runner))
              (skip (test-runner-skip-count runner))
              (total (+ pass fail xfail xpass skip)))
         (format #t "~%Test Results~%============~%~%\
Passed: ~a~%Failed: ~a~%\
Expectedly failed: ~a~%Unexpectedly passed: ~a~%\
Skipped: ~a~%Total: ~a~%~%"
                 (number->string pass) (number->string fail)
                 (number->string xfail) (number->string xpass)
                 (number->string skip) (number->string total))
         (force-output))))
   runner))
