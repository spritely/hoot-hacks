;;; Copyright 2019-2021 Christine Lemmer-Webber
;;; Copyright 2024 Juliana Sims
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.


;; STAGE 3: Add:
;;  - <-np
;;  - most of the mactors
;;
;; Breaking things into stages is starting to get a bit messy as I run
;; out of time...

(library (pre-goblins goblins-stage3)
  (export *unspecified*
          make-whactormap
          make-actormap

          spawn $

          actormap-spawn
          actormap-spawn!

          actormap-turn*
          actormap-turn

          actormap-peek
          actormap-poke!
          actormap-reckless-poke!

          actormap-run
          actormap-run!
          actormap-run*

          whactormap?
          transactormap?
          transactormap-merge!

          <-np)
  (import (hoot hashtables)
          (hoot match)
          (only (hoot syntax) define*)
          (scheme base)
          (scheme write))

  
;;;                  .============================.
;;;                  | High level view of Goblins |
;;;                  '============================'
;;;
;;; There's a lot of architecture here.
;;; It's a lot to take in, so let's start out with a "high level view".
;;; Here's an image to get started:
;;;
;;;   .----------------------------------.         .-------------------.
;;;   |              Node 1              |         |       Node 2      |
;;;   |             =======              |         |       =======     |
;;;   |                                  |         |                   |
;;;   | .--------------.  .---------.   .-.       .-.                  |
;;;   | |    Vat A     |  |  Vat B  |   |  \______|  \_   .----------. |
;;;   | |  .---.       |  |   .-.   | .-|  /      |  / |  |   Vat C  | |
;;;   | | (Alice)----------->(Bob)----' '-'       '-'  |  |  .---.   | |
;;;   | |  '---'       |  |   '-'   |    |         |   '--->(Carol)  | |
;;;   | |      \       |  '----^----'    |         |      |  '---'   | |
;;;   | |       V      |       |         |         |      |          | |
;;;   | |      .----.  |       |        .-.       .-.     |  .----.  | |
;;;   | |     (Alfred) |       '-------/  |______/  |____---(Carlos) | |
;;;   | |      '----'  |               \  |      \  |     |  '----'  | |
;;;   | |              |                '-'       '-'     '----------' |
;;;   | '--------------'                 |         |                   |
;;;   |                                  |         |                   |
;;;   '----------------------------------'         '-------------------'
;;;
;;; Here we see the following:
;;;
;;;  - Zooming in the farthest, we are looking at the "object layer"...
;;;    Alice has a reference to Alfred and Bob, Bob has a reference to Carol,
;;;    Carlos has a reference to Bob.  Reference possession is directional;
;;;    even though Alice has a reference to Bob, Bob does not have a
;;;    reference to Alice.
;;;
;;;  - One layer up is the "vat layer"... here we can see that Alice and
;;;    Alfred are both objects in Vat A, Bob is an object in Vat B, and
;;;    Carol and Carlos are objects in Vat C.
;;;
;;;  - Zooming out the farthest is the "node/network level".
;;;    There are two nodes (Node 1 and Node 2) connected over a
;;;    Goblins CapTP network.  The stubby shapes on the borders between the
;;;    nodes represent the directions of references Node 1 has to
;;;    objects in Node 2 (at the top) and references Node 2 has to
;;;    Node 1.  Both nodes in this diagram are cooperating to preserve
;;;    that Bob has access to Carol but that Carol does not have access to
;;;    Bob, and that Carlos has access to Bob but Bob does not have access
;;;    to Carlos.  (However there is no strict guarantee from either
;;;    node's perspective that this is the case... generally it's in
;;;    everyone's best interests to take a "principle of least authority"
;;;    approach though so usually it is.)
;;;
;;; This illustration is what's sometimes called a "grannovetter diagram"
;;; in the ocap community, inspired by the kinds of diagrams in Mark
;;; S. Grannovetter's "The Strength of Weak Ties" paper.  The connection is
;;; that while the "Weak Ties" paper was describing the kinds of social
;;; connections between people (Alice knows Bob, Bob knows Carol), similar
;;; patterns arise in ocap systems (the object Alice has a refernce to Bob,
;;; and Bob has a reference to Carol).
;;;
;;; With that in mind, we're now ready to look at things more structurally.
                                        ;
                                        ;
                                        ;                  .============================.
                                        ;                  | Goblins abstraction layers |
                                        ;                  '============================'
                                        ;
                                        ; Generally, things look like so:
                                        ;
;;;   (node (vat (actormap {refr: (mactor object-handler)})))
;;;
;;; However, we could really benefit from looking at those in more detail,
;;; so from the outermost layer in...
;;;
;;;    .--- A node in Goblins is basically an OS process.
;;;    |    However, the broader Goblins CapTP/MachineTP network is
;;;    |    made up of many nodes.  A connection to another node
;;;    |    is the closest amount of "assurance" a Goblins node has
;;;    |    that it is delivering to a specific destination.
;;;    |    Nonetheless, Goblins users generally operate at the object
;;;    |    reference level of abstraction, even across nodes.
;;;    |
;;;    |    An object reference on the same node is considered
;;;    |    "local" and an object reference on another node is
;;;    |    considered "remote".
;;;    |
;;;    |    .--- Christine: "How about I call this 'hive'?"
;;;    |    |    Ocap community: "We hate that, use 'vat'"
;;;    |    |    Everyone else: "What's a 'vat' what a weird name"
;;;    |    |
;;;    |    |    A vat is a traditional ocap term, both a container for
;;;    |    |    objects but most importantly an event loop that
;;;    |    |    communicates with other event loops.  Vats operate
;;;    |    |    "one turn at a time"... a toplevel message is handled
;;;    |    |    for some object which is transactional; either it happens
;;;    |    |    or, if something bad happens in-between, no effects occur
;;;    |    |    at all (except that a promise waiting for the result of
;;;    |    |    this turn is broken).
;;;    |    |
;;;    |    |    Objects in the same vat are "near", whereas objects in
;;;    |    |    remote vats are "far".  (As you may notice, "near" objects
;;;    |    |    can be "near" or "far", but "remote" objects are always
;;;    |    |    "far".)
;;;    |    |
;;;    |    |    This distinction is important, because Goblins supports
;;;    |    |    both asynchronous messages + promises via `<-` and
;;;    |    |    classic synchronous call-and-return invocations via `$`.
;;;    |    |    However, while any actor can call any other actor via
;;;    |    |    <-, only near actors may use $ for synchronous call-retun
;;;    |    |    invocations.  In the general case, a turn starts by
;;;    |    |    delivering to an actor in some vat a message passed with <-,
;;;    |    |    but during that turn many other near actors may be called
;;;    |    |    with $.  For example, this allows for implementing transactional
;;;    |    |    actions as transferring money from one account/purse to another
;;;    |    |    with $ in the same vat very easily, while knowing that if
;;;    |    |    something bad happens in this transaction, no actor state
;;;    |    |    changes will be committed (though listeners waiting for
;;;    |    |    the result of its transaction will be informed of its failure);
;;;    |    |    ie, the financial system will not end up in a corrupt state.
;;;    |    |    In this example, it is possible for users all over the network
;;;    |    |    to hold and use purses in this vat, even though this vat is
;;;    |    |    responsible for money transfer between those purses.
;;;    |    |    For an example of such a financial system in E, see
;;;    |    |    "An Ode to the Grannovetter Diagram":
;;;    |    |      http://erights.org/elib/capability/ode/index.html
;;;    |    |
;;;    |    |    .--- Earlier we said that vats are both an event loop and
;;;    |    |    |    a container for storing actor state.  Surprise!  The
;;;    |    |    |    vat is actually wrapping the container, which is called
;;;    |    |    |    an "actormap".  While vats do not expose their actormaps,
;;;    |    |    |    Goblins has made a novel change by allowing actormaps to
;;;    |    |    |    be used as independent first-class objects.  Most users
;;;    |    |    |    will rarely do this, but first-class usage of actormaps
;;;    |    |    |    is still useful if integrating Goblins with an existing
;;;    |    |    |    event loop (such as one for a video game or a GUI) or for
;;;    |    |    |    writing unit tests.
;;;    |    |    |
;;;    |    |    |    The keys to actormaps are references (called "refrs")
;;;    |    |    |    and the values are current behavior.  This is described
;;;    |    |    |    below.
;;;    |    |    |
;;;    |    |    |    Actormaps also technically operate on "turns", which are
;;;    |    |    |    a transactional operation.  Once a turn begins, a dynamic
;;;    |    |    |    "syscaller" (or "actor context") is initialized so that
;;;    |    |    |    actors can make changes within this transaction.  At the
;;;    |    |    |    end of the turn, the user of actormap-turn is presented
;;;    |    |    |    with the transactional actormap (called "transactormap")
;;;    |    |    |    which can either be committed or not to the current mutable
;;;    |    |    |    actormap state ("whactormap", which stands for
;;;    |    |    |    "weak hash actormap"), alongside a queue of messages that
;;;    |    |    |    were scheduled to be run from actors in this turn using <-,
;;;    |    |    |    and the result of the computation run.
;;;    |    |    |
;;;    |    |    |    However, few users will operate using actormap-turn directly,
;;;    |    |    |    and will instead either use actormap-poke! (which automatically
;;;    |    |    |    commits the transaction if it succeeds or propagates the error)
;;;    |    |    |    or actormap-peek (which returns the result but throws away the
;;;    |    |    |    transaction; useful for getting a sense of what's going on
;;;    |    |    |    without committing any changes to actor state).
;;;    |    |    |    Or, even more commonly, they'll just use a vat and never think
;;;    |    |    |    about actormaps at all.
;;;    |    |    |
;;;    |    |    |         .--- A reference to an object or actor.
;;;    |    |    |         |    Traditionally called a "ref" by the ocap community, but
;;;    |    |    |         |    scheme already uses "-ref" everywhere so we call it
;;;    |    |    |         |    "refr" instead.  Whatever.
;;;    |    |    |         |
;;;    |    |    |         |    Anyway, these are the real "capabilities" of Goblins'
;;;    |    |    |         |    "object capability system".  Holding onto one gives you
;;;    |    |    |         |    authority to make invocations with <- or $, and can be
;;;    |    |    |         |    passed around to procedure or actor invocations.
;;;    |    |    |         |    Effectively the "moral equivalent" of a procedure
;;;    |    |    |         |    reference.  If you have it, you can use (and share) it;
;;;    |    |    |         |    if not, you can't.
;;;    |    |    |         |
;;;    |    |    |         |    Actually, technically these are local-live-refrs...
;;;    |    |    |         |    see "The World of Refrs" below for the rest of them.
;;;    |    |    |         |
;;;    |    |    |         |      .--- We're now at the "object behavior" side of
;;;    |    |    |         |      |    things.  I wish I could avoid talking about
;;;    |    |    |         |      |    "mactors" but we're talking about the actual
;;;    |    |    |         |      |    implementation here so... "mactor" stands for
;;;    |    |    |         |      |    "meta-actor", and really there are a few
;;;    |    |    |         |      |    "core kinds of behavior" (mainly for promises
;;;    |    |    |         |      |    vs object behavior).  But in the general case,
;;;    |    |    |         |      |    most objects from a user's perspective are the
;;;    |    |    |         |      |    mactor:object kind, which is just a wrapper
;;;    |    |    |         |      |    around the current object handler (as well as
;;;    |    |    |         |      |    some information to track when this object is
;;;    |    |    |         |      |    "becoming" another kind of object.
;;;    |    |    |         |      |
;;;    |    |    |         |      |      .--- Finally, "object"... a term that is
;;;    |    |    |         |      |      |    unambiguous and well-understood!  Well,
;;;    |    |    |         |      |      |    "object" in our system means "references
;;;    |    |    |         |      |      |    mapping to an encapsulation of state".
;;;    |    |    |         |      |      |    Refrs are the reference part, so
;;;    |    |    |         |      |      |    object-handlers are the "current state"
;;;    |    |    |         |      |      |    part.  The time when an object transitions
;;;    |    |    |         |      |      |    from "one" behavior to another is when it
;;;    |    |    |         |      |      |    returns a new handler wrapped in a "become"
;;;    |    |    |         |      |      |    wrapper specific to this object (and
;;;    |    |    |         |      |      |    provided to the object at construction
;;;    |    |    |         |      |      |    time)
;;;    |    |    |         |      |      |
;;;    V    V    V         V      V      V
;;; (node (vat (actormap {refr: (mactor object-handler)})))
;;;
;;;
;;; Whew!  That's a lot of info, so go take a break and then we'll go onto
;;; the next section.
;;;
;;;
;;;                     .====================.
;;;                     | The World of Refrs |
;;;                     '===================='
;;;
;;; There are a few kinds of references, explained below:
;;;
;;;                                     live refrs :
;;;                     (runtime or captp session) : offline-storeable
;;;                     ========================== : =================
;;;                                                :
;;;                local?           remote?        :
;;;           .----------------.----------------.  :
;;;   object? | local-object   | remote-object  |  :    [sturdy refrs]
;;;           |----------------+----------------|  :
;;;  promise? | local-promise  | remote-promise |  :     [cert chains]
;;;           '----------------'----------------'  :
;;;
;;; On the left hand side we see live references (only valid within this
;;; process runtime or between nodes across captp sessions) and
;;; offline-storeable references (sturdy refrs, a kind of bearer URI,
;;; and certificate chains, which are like "deeds" indicating that the
;;; possessor of some cryptographic material is permitted access).
;;;
;;; All offline-storeable references must first be converted to live
;;; references before they can be used (authority to do this itself a
;;; capability, as well as authority to produce these offline-storeable
;;; objects).
;;;
;;; Live references subdivide into local (on the same node) and
;;; remote (on a foreign node).  These are typed as either
;;; representing an object or a promise.
;;;
;;; (Local references also further subdivide into "near" and "far",
;;; but rather than being encoded in the reference type this is
;;; determined relative to another local-refr or the current actor
;;; context.)

  (define *unspecified* (if #f #f))

  
  ;; Actormaps, etc
  ;; ==============

  (define-record-type <actormap>
    ;; TODO: This is confusing, naming-wise? (see make-actormap alias)
    (_make-actormap metatype data vat-connector)
    actormap?
    (metatype actormap-metatype)
    (data actormap-data)
    (vat-connector actormap-vat-connector))

  (define-record-type <actormap-metatype>
    (make-actormap-metatype name ref-proc set!-proc)
    actormap-metatype?
    (name actormap-metatype-name)
    (ref-proc actormap-metatype-ref-proc)
    (set!-proc actormap-metatype-set!-proc))

  (define (actormap-set! am key val)
    ((actormap-metatype-set!-proc (actormap-metatype am))
     am key val)
    *unspecified*)

  ;; (-> actormap? local-refr? (or/c mactor? #f))
  (define (actormap-ref am key)
    ((actormap-metatype-ref-proc (actormap-metatype am)) am key))

  ;; Weak-hash actormaps
  ;; ===================

  (define-record-type <whactormap-data>
    (make-whactormap-data wht)
    whactormap-data?
    (wht whactormap-data-wht))

  (define (whactormap-ref am key)
    (define wht (whactormap-data-wht (actormap-data am)))
    (weak-key-hashtable-ref wht key #f))

  (define (whactormap-set! am key val)
    (define wht (whactormap-data-wht (actormap-data am)))
    (weak-key-hashtable-set! wht key val))

  (define whactormap-metatype
    (make-actormap-metatype 'whactormap whactormap-ref whactormap-set!))

  (define* (make-whactormap #:key [vat-connector #f])
    (_make-actormap whactormap-metatype
                    (make-whactormap-data (make-weak-key-hashtable))
                    vat-connector))

  (define (whactormap? obj)
    (and (actormap? obj)
         (eq? (actormap-metatype obj) whactormap-metatype)))

  ;; TODO: again, confusing (see <actormap>)
  (define make-actormap make-whactormap)


  
  ;; Transactional actormaps
  ;; =======================

  (define-record-type <transactormap-data>
    (make-transactormap-data parent delta merged?)
    transactormap-data?
    (parent transactormap-data-parent)
    (delta transactormap-data-delta)
    (merged? transactormap-data-merged? set-transactormap-data-merged?!))

  (define (transactormap-merged? transactormap)
    (transactormap-data-merged? (actormap-data transactormap)))

  (define (transactormap-ref transactormap key)
    (define tm-data (actormap-data transactormap))
    (when (transactormap-data-merged? tm-data)
      (error "Can't use transactormap-ref on merged transactormap"))
    (define tm-delta
      (transactormap-data-delta tm-data))
    (define tm-val (hashtable-ref tm-delta key #f))
    (if tm-val
        ;; we got it, it's in our delta
        tm-val
        ;; search parents for key
        (let ([parent (transactormap-data-parent tm-data)])
          (actormap-ref parent key))))

  (define (transactormap-set! transactormap key val)
    (when (transactormap-merged? transactormap)
      (error "Can't use transactormap-set! on merged transactormap"))
    (define tm-delta (transactormap-data-delta (actormap-data transactormap)))
    (hashtable-set! tm-delta key val)
    *unspecified*)

  ;; Not threadsafe, but probably doesn't matter
  (define (transactormap-merge! transactormap)
    ;; Serves two functions:
    ;;  - to extract the root weak-hasheq
    ;;  - to merge this transaction on top of the weak-hasheq
    (define (do-merge! transactormap)
      (define tm-data (actormap-data transactormap))
      (define parent (transactormap-data-parent tm-data))
      (define parent-mtype (actormap-metatype parent))
      ;; TODO: Should we actually return the root-wht instead,
      ;;   since that's what we're comitting to?
      (define root-actormap
        (cond
         [(eq? parent-mtype whactormap-metatype)
          parent]
         [(eq? parent-mtype transactormap)
          (do-merge! parent)]
         [else
          (error (string-append "Actormap metatype not support for merging: "
                                parent-mtype))]))
      ;; Optimization: we pull out the root weak hash table here and
      ;; merge it
      (define root-wht (whactormap-data-wht (actormap-data root-actormap)))
      (unless (transactormap-data-merged? tm-data)
        (hashtable-for-each
         (lambda (key val)
           (weak-key-hashtable-set! root-wht key val))
         (transactormap-data-delta tm-data))
        (set-transactormap-data-merged?! tm-data #t))
      root-actormap)
    (do-merge! transactormap)
    *unspecified*)

  (define transactormap-metatype
    (make-actormap-metatype 'transactormap transactormap-ref transactormap-set!))

  (define (make-transactormap parent)
    (define vat-connector (actormap-vat-connector parent))
    (_make-actormap transactormap-metatype
                    (make-transactormap-data parent (make-eq-hashtable) #f)
                    vat-connector))


  
  ;; Ref(r)s
  ;; =======

  (define-record-type <local-object-refr>
    (make-local-object-refr debug-name vat-connector)
    local-object-refr?
    (debug-name local-object-refr-debug-name)
    (vat-connector local-object-refr-vat-connector))

  (define-record-type <local-promise-refr>
    (make-local-promise-refr vat-connector)
    local-promise-refr?
    (vat-connector local-promise-refr-vat-connector))

  (define (local-refr? obj)
    (or (local-object-refr? obj) (local-promise-refr? obj)))

  (define (local-refr-vat-connector local-refr)
    (match local-refr
      [(? local-object-refr?)
       (local-object-refr-vat-connector local-refr)]
      [(? local-promise-refr?)
       (local-promise-refr-vat-connector local-refr)]))


  (define (live-refr? obj)
    (or (local-refr? obj)))


  
  ;; "Become" sealer/unsealers
  ;; =========================

  (define (make-become-sealer-triplet)
    (define-record-type <become-seal>
      (make-become-seal new-behavior return-val)
      become-sealed?
      (new-behavior unseal-behavior)
      (return-val unseal-return-val))
    (define* (become new-behavior #:optional [return-val *unspecified*])
      (make-become-seal new-behavior return-val))
    (define (unseal sealed)
      (values (unseal-behavior sealed)
              (unseal-return-val sealed)))
    (values become unseal become-sealed?))


  
  ;; Mactors
  ;; =======

;;;                    .======================.
;;;                    | The World of Mactors |
;;;                    '======================'
;;;
;;; This is getting really deep into the weeds and is really only
;;; relevant to anyone hacking on this module.
;;;
;;; Mactors are only ever relevant to the internals of a vat, but they
;;; do define some common behaviors.
;;;
;;; Here are the categories and transition states:
;;;
;;;        Unresolved                     Resolved
;;;  __________________________  ___________________________
;;; |                          ||                           |
;;;
;;;                 .----------------->.    .-->[object]
;;;                 |                  |    |
;;;                 |    .--.          |    +-->[local-link]
;;;     [naive]-->. |    v  |          |    |
;;;               +>+->[closer]------->'--->+-->[encased]
;;;  [question]-->' |       |               |
;;;                 |       |               '-->[broken]
;;;                 '------>'--->[remote-link]    ^
;;;                                  |            |
;;;                                  '----------->'
;;;
;;; |________________________________________||_____________|
;;;                  Eventual                     Settled
;;;
;;; The four major categories of mactors:
;;;
;;;  - Unresolved: A promise that has never been fulfilled or broken.
;;;  - Resolved: Either an object with its own handler or a promise which
;;;    has been fulfilled to some value/object reference or which has broken.
;;;
;;; and:
;;;
;;;  - Eventual: Something which *might* eventually transition its state.
;;;  - Settled: Something which will never transition its state again.
;;;
;;; The surprising thing here is that there is any distinction between
;;; unresolved/resolved and eventual/settled at all.  The key to
;;; understanding the difference is observing that a mactor:remote-link
;;; might become broken upon network disconnect from that object.
;;;
;;; One intersting observation is that if you have a local-object-refr that
;;; it is sure to correspond to a mactor:object.  A local-promise-refr can
;;; correspond to any object state *except* for mactor:object (if a promise
;;; resolves to a local object, it must point to it via mactor:local-link.)
;;; (remote-refrs of course never correspond to a mactor on this machine;
;;; those are managed by captp.)
;;;
;;; See also:
;;;  - The comments above each of these below
;;;  - "Miranda methods":
;;;      http://www.erights.org/elang/blocks/miranda.html
;;;  - "Reference mechanics":
;;;      http://erights.org/elib/concurrency/refmech.html

  ;; local-objects are the most common type, have a message handler
  ;; which specifies how to respond to the next message, as well as
  ;; a predicate and unsealer to identify and unpack when a message
  ;; handler specifies that this actor would like to "become" a new
  ;; version of itself (get a new handler)
  (define-record-type <mactor:object>
    (mactor:object behavior become-unsealer become?)
    mactor:object?
    (behavior mactor:object-behavior)
    (become-unsealer mactor:object-become-unsealer)
    (become? mactor:object-become?))

  ;; The other kinds of mactors correspond to promises and their resolutions.

  ;; There are two supertypes here which are not used directly:
  ;; mactor:unresolved and mactor:eventual.  See above for an explaination
  ;; of what these mean.
  ;; These are never directly exposed as mactors, hence the ~
  (define-record-type <m~eventual>
    (make-m~eventual resolver-unsealer resolver-tm?)
    m~eventual?
    ;; We can still be resolved, so identify who is allowed to do that
    ;; and provide a mechanism for unsealing the resolution
    (resolver-unsealer m~eventual-resolver-unsealer)
    (resolver-tm? m~eventual-resolver-tm?))
  (define-record-type <m~unresolved>
    (make-m~unresolved eventual listeners)
    m~unresolved?
    ;; the <m~eventual> info
    (eventual m~unresolved-eventual)
    ;; Who's listening for a resolution?
    (listeners m~unresolved-listeners))

  ;; The most common kind of freshly made promise is a naive one.
  ;; It knows no interesting information about how what it will eventually
  ;; become.
  ;; Since it knows of no closer information it keeps a queue of waiting
  ;; messages which will eventually be transmitted.
  (define-record-type <mactor:naive>
    (make-mactor:naive unresolved waiting-messages)
    mactor:naive?
    (unresolved mactor:naive-unresolved)
    ;; All of these get "rewritten" as this promise is either resolved
    ;; or moved closer to resolution.
    (waiting-messages mactor:naive-waiting-messages))

  ;; A special kind of "freshly made" promise which also corresponds to being
  ;; a question on the remote end.  Keeps track of the captp-connector
  ;; relevant to this connection so it can send it messages and the
  ;; question-finder that it corresponds to (used for passing along messages).
  (define-record-type <mactor:question>
    (make-mactor:question unresolved captp-connector question-finder)
    mactor:question?
    (unresolved mactor:question-unresolved)
    (captp-connector mactor:question-captp-connector)
    (question-finder mactor:question-question-finder))

  ;; "You make me closer to God" -- Nine Inch Nails
  ;; Well, in this case we're actually just "closer to resolution"...
  ;; pointing at some other promise that isn't us.
  (define-record-type <mactor:closer>
    (make-mactor:closer unresolved point-to history waiting-messages)
    mactor:closer?
    (unresolved mactor:closer-unresolved)
    ;; Who do we currently point to?
    (point-to mactor:closer-point-to)
    ;; A set of promises we used to point to before they themselves
    ;; resolved... used to detect cycles
    (history mactor:closer-history)
    ;; Any messages that are waiting to be passed along...
    ;; Currently only if we're pointing to a remote-promise, otherwise
    ;; this will be an empty list.
    (waiting-messages mactor:closer-waiting-messages))

  ;; Point at a remote object.
  ;; It's eventual because, well, it could still break on network partition.
  (define-record-type <mactor:remote-link>
    (make-mactor:remote-link eventual point-to)
    mactor:remote-link?
    (eventual mactor:remote-link-eventual)
    (point-to mactor:remote-link-point-to))

  ;; Link to an object on the same machine.
  (define-record-type <mactor:local-link>
    (make-mactor:local-link point-to)
    mactor:local-link?
    (point-to mactor:local-link-point-to))

  ;; A promise that has resolved to some value
  (define-record-type <mactor:encased>
    (make-mactor:encased val)
    mactor:encased?
    (val mactor:encased-val))

  ;; Breakage (and remember why!)
  (define-record-type <mactor:broken>
    (make-mactor:broken problem)
    mactor:broken?
    (problem mactor:broken-problem))

  ;; Rather than directly storing references to listeners, we use these
  ;; <listener-info> structs because, at least at the time, we have this
  ;; notion of being interested in "partial" updates (rather than waiting
  ;; until full promise resolution)
  ;;
  ;; While this is a curious feature, we never fully documented why we
  ;; made the decision to enable this.  It would be interesting to document
  ;; it, and we probably will indeed need to for ocapn interoperability.
  (define-record-type <listener-info>
    (make-listener-info resolve-me wants-partial?)
    listener-info?
    (resolve-me listener-info-resolve-me)
    (wants-partial? listener-info-wants-partial?))

  (define (mactor:eventual? obj)
    (or (mactor:remote-link? obj)
        (mactor:unresolved? obj)))
  (define (mactor:unresolved? obj)
    (or (mactor:naive? obj)
        (mactor:question? obj)
        (mactor:closer? obj)))

  (define (mactor-get-m~unresolved obj)
    (match obj
      [(? mactor:naive?) (mactor:naive-unresolved obj)]
      [(? mactor:question?) (mactor:question-unresolved obj)]
      [(? mactor:closer?) (mactor:closer-unresolved obj)]))

  (define (mactor-get-m~eventual obj)
    (match obj
      [(? mactor:unresolved? obj)
       (m~unresolved-eventual (mactor-get-m~unresolved obj))]
      [(? mactor:remote-link? obj)
       (mactor:remote-link-eventual obj)]))

  (define (mactor:unresolved-add-listener mactor new-listener wants-partial?)
    (define new-listener-info
      (make-listener-info new-listener wants-partial?))
    (define new-unresolved
      (match (mactor-get-m~unresolved mactor)
        [($ <m~unresolved> eventual listeners)
         (make-m~unresolved eventual (cons new-listener-info listeners))]))
    (match mactor
      [($ <mactor:naive> unresolved waiting-messages)
       (make-mactor:naive new-unresolved waiting-messages)]
      [($ <mactor:question> unresolved captp-connector question-finder)
       (make-mactor:question new-unresolved captp-connector question-finder)]
      [($ <mactor:closer> unresolved point-to history waiting-messages)
       (make-mactor:closer new-unresolved point-to history waiting-messages)]))

  ;; Helper for syscaller's fulfill-promise and break-promise methods
  (define (unseal-mactor-resolution mactor sealed-resolution)
    (define eventual (mactor-get-m~eventual mactor))
    (define resolver-tm?
      (m~eventual-resolver-tm? eventual))
    (define resolver-unsealer
      (m~eventual-resolver-unsealer eventual))
    ;; Is this a valid resolution?
    (unless (resolver-tm? sealed-resolution)
      (error "Resolution sealed with wrong trademark!"))
    (resolver-unsealer sealed-resolution))


  

  ;; Messages
  ;; --------

  ;; These are the main things that get sent as the toplevel of a turn in a vat!
  (define-record-type <message>
    (make-message to resolve-me args answer-this-question)
    message?
    ;; who's receiving the message (the invoked actor)
    (to message-to)
    ;; who's interested in the result (a resolver)
    (resolve-me message-resolve-me)
    ;; arguments to the invoked actor
    (args message-args)
    ;; Either a question-finder or #f
    (answer-this-question message-answer-this-question))

  (define (question-message? msg)
    (if (message-answer-this-question msg) #t #f))


  
  ;; Syscaller
  ;; =========

  ;; Do NOT export this esp under serious ocap confinement
  (define current-syscaller (make-parameter #f))

  (define (fresh-syscaller actormap)
    (define vat-connector
      (actormap-vat-connector actormap))
    (define new-msgs '())

    (define closed? #f)

    (define (this-syscaller method-id . args)
      (when closed?
        (error "Sorry, this syscaller is closed for business!"))
      (define method
        (case method-id
          [($) _$]
          [(spawn) _spawn]
          ;; [(<-) _<-]
          [(<-np) _<-np]
          [(spawn-mactor) spawn-mactor]
          [(send-message) _send-message]
          [(vat-connector) get-vat-connector]
          [(near-refr?) near-refr?]
          [(near-mactor) near-mactor]
          [else (error 'invalid-syscaller-method
                       "~a" method-id)]))
      (apply method args))

    (define (near-refr? obj)
      (and (local-refr? obj)
           (eq? (local-refr-vat-connector obj)
                vat-connector)))

    (define (near-mactor refr)
      (actormap-ref actormap refr))

    (define (get-vat-connector)
      vat-connector)

    (define (actormap-ref-or-die to-refr)
      (define mactor
        (actormap-ref actormap to-refr))
      (unless mactor
        (error 'no-such-actor "no actor with this id in this vat: ~a" to-refr))
      mactor)

    ;; call actor's behavior
    (define (_$ to-refr args)
      ;; Restrict to live-refrs which appear to have the same
      ;; vat-connector as us
      (unless (local-refr? to-refr)
        (error 'not-callable
               "Not a live reference: ~a" to-refr))

      (unless (eq? (local-refr-vat-connector to-refr)
                   vat-connector)
        (error 'not-callable
               "Not in the same vat: ~a" to-refr))

      (define mactor
        (actormap-ref-or-die to-refr))

      (match mactor
        [(? mactor:object?)
         (let ((actor-behavior
                (mactor:object-behavior mactor))
               (become?
                (mactor:object-become? mactor))
               (become-unsealer
                (mactor:object-become-unsealer mactor)))
           ;; I guess watching for this guarantees that an immediate call
           ;; against a local actor will not be tail recursive.
           ;; TODO: We need to document that.
           (define-values (new-behavior return-val)
             (let ([returned (apply actor-behavior args)])
               (if (become? returned)
                   ;; The unsealer unseals both the behavior and return-value anyway
                   (become-unsealer returned)
                   ;; In this case, we're not becoming anything, so just give us
                   ;; the return-val
                   (values #f returned))))

           ;; if a new behavior for this actor was specified,
           ;; let's replace it
           (when new-behavior
             (unless (procedure? new-behavior)
               (error 'become-failure "Tried to become a non-procedure behavior:"
                      new-behavior))
             (actormap-set! actormap to-refr
                            (mactor:object
                             new-behavior
                             (mactor:object-become-unsealer mactor)
                             (mactor:object-become? mactor))))

           return-val)]
        ;; If it's an encased value, "calling" it just returns the
        ;; internal value.
        [(? mactor:encased?)
         (mactor:encased-val mactor)]
        ;; Ah... we're linking to another actor locally, so let's
        ;; just de-symlink and call that instead.
        [(? mactor:local-link?)
         (apply _$ (mactor:local-link-point-to mactor)
                args)]
        ;; Not a callable mactor!
        [_other
         (error 'not-callable
                "Not an encased or object mactor: ~a" mactor)]))

    ;; spawn a new actor
    (define (_spawn constructor args debug-name)
      (define-values (become become-unsealer become-sealed?)
        (make-become-sealer-triplet))
      (define initial-behavior
        (apply constructor become args))
      (match initial-behavior
        ;; New procedure, so let's set it
        [(? procedure?)
         (let ((actor-refr
                (make-local-object-refr debug-name vat-connector)))
           (actormap-set! actormap actor-refr
                          (mactor:object initial-behavior
                                         become-unsealer become-sealed?))
           actor-refr)]
        ;; If someone returns another actor, just let that be the actor
        [(? live-refr? pre-existing-refr)
         pre-existing-refr]
        [_
         (error 'invalid-actor-handler "Not a procedure or live refr: ~a" initial-behavior)]))

    (define (spawn-mactor mactor debug-name)
      (actormap-spawn-mactor! actormap mactor debug-name))

    ;; helper to the below two methods
    (define* (_send-message to-refr resolve-me args
                            #:key [answer-this-question #f])
      (unless (live-refr? to-refr)
        (error 'send-message
               "Don't know how to send a message to:" to-refr))
      (define new-message
        (if answer-this-question
            (make-message to-refr resolve-me args answer-this-question)
            (make-message to-refr resolve-me args #f)))
      (set! new-msgs (cons new-message new-msgs)))

    (define (_<-np to-refr args)
      (_send-message to-refr #f args)
      *unspecified*)

    (define (get-internals)
      (list actormap new-msgs))

    (define (close-up!)
      (set! closed? #t))

    (values this-syscaller get-internals close-up!))

  (define (call-with-fresh-syscaller am proc)
    (define-values (sys get-sys-internals close-up!)
      (fresh-syscaller am))
    (dynamic-wind
      (lambda () #f)
      (lambda ()
        (parameterize ([current-syscaller sys])
          (proc sys get-sys-internals)))
      (lambda ()
        (close-up!))))

  (define (get-syscaller-or-die)
    (define sys (current-syscaller))
    (unless sys
      (error "No current syscaller"))
    sys)


  
  ;; Core API
  ;; ========

  ;; System calls
  (define (spawn constructor . args)
    (define sys (get-syscaller-or-die))
    (sys 'spawn constructor args 'procedure-name-unsupported))
  (define ($ refr . args)
    (define sys (get-syscaller-or-die))
    (sys '$ refr args))
  (define (<- refr . args)
    (define sys (get-syscaller-or-die))
    (sys '<- refr args))
  (define (<-np refr . args)
    (define sys (get-syscaller-or-die))
    (sys '<-np refr args))


  
  ;; Spawning
  ;; ========

  ;; This is the internally used version of actormap-spawn,
  ;; also used by the syscaller.  It doesn't set up a syscaller
  ;; if there isn't currently one.
  (define* (actormap-spawn!* actormap actor-constructor
                             args
                             #:optional
                             [debug-name 'procedure-name-unsupported])
    (define vat-connector
      (actormap-vat-connector actormap))
    (define-values (become become-unseal become?)
      (make-become-sealer-triplet))
    (define actor-handler
      (apply actor-constructor become args))
    (match actor-handler
      ;; New procedure, so let's set it
      [(? procedure?)
       (let ((actor-refr
              (make-local-object-refr debug-name vat-connector)))
         (actormap-set! actormap actor-refr
                        (mactor:object actor-handler
                                       become-unseal become?))
         actor-refr)]
      [(? live-refr? pre-existing-refr)
       pre-existing-refr]
      [_
       (error 'invalid-actor-handler "Not a procedure or live refr: ~a" actor-handler)]))

  ;; These two are user-facing procedures.  Thus, they set up
  ;; their own syscaller.

  ;; non-committal version of actormap-spawn
  (define (actormap-spawn actormap actor-constructor . args)
    (define new-actormap
      (make-transactormap actormap))
    (call-with-fresh-syscaller
     new-actormap
     (lambda (sys get-sys-internals)
       (define actor-refr
         (actormap-spawn!* new-actormap actor-constructor
                           args))
       (values actor-refr new-actormap))))

  (define (actormap-spawn! actormap actor-constructor . args)
    (define new-actormap
      (make-transactormap actormap))
    (define actor-refr
      (call-with-fresh-syscaller
       new-actormap
       (lambda (sys get-sys-internals)
         (actormap-spawn!* new-actormap actor-constructor args))))
    (transactormap-merge! new-actormap)
    actor-refr)

  (define* (actormap-spawn-mactor! actormap mactor
                                   #:optional
                                   [debug-name #f])
    (define vat-connector
      (actormap-vat-connector actormap))
    (define actor-refr
      (if (mactor:object? mactor)
          (make-local-object-refr debug-name vat-connector)
          (make-local-promise-refr vat-connector)))
    (actormap-set! actormap actor-refr mactor)
    actor-refr)


  
;;; actormap turning and utils
;;; ==========================

  (define (actormap-turn* actormap to-refr args)
    (call-with-fresh-syscaller
     actormap
     (lambda (sys get-sys-internals)
       (define result-val
         (sys '$ to-refr args))
       (apply values result-val
              (get-sys-internals)))))   ; actormap new-msgs

  (define (actormap-turn actormap to-refr . args)
    (define new-actormap
      (make-transactormap actormap))
    (actormap-turn* new-actormap to-refr args))

  ;; run a turn but only for getting the result.
  ;; we're not interested in committing the result
  ;; so we discard everything but the result.
  (define (actormap-peek actormap to-refr . args)
    (define-values (returned-val _am _nm)
      (actormap-turn* (make-transactormap actormap)
                      to-refr args))
    returned-val)

  ;; Note that this does nothing with the messages.
  (define (actormap-poke! actormap to-refr . args)
    (define-values (returned-val transactormap _nm)
      (actormap-turn* (make-transactormap actormap)
                      to-refr args))
    (transactormap-merge! transactormap)
    returned-val)

  (define (actormap-reckless-poke! actormap to-refr . args)
    (define-values (returned-val _am _nm)
      (actormap-turn* actormap to-refr args))
    returned-val)

  ;; like actormap-run but also returns the new actormap, new-msgs
  (define (actormap-run* actormap thunk)
    (define-values (actor-refr new-actormap)
      (actormap-spawn (make-transactormap actormap) (lambda (_bcom) thunk)))
    (define-values (returned-val new-actormap2 new-msgs)
      (actormap-turn* (make-transactormap new-actormap) actor-refr '()))
    (values returned-val new-actormap2 new-msgs))

  ;; non-committal version of actormap-run
  (define (actormap-run actormap thunk)
    (define-values (returned-val _am _nm)
      (actormap-run* (make-transactormap actormap) thunk))
    returned-val)

  ;; committal version
  ;; Run, and also commit the results of, the code in the thunk
  (define* (actormap-run! actormap thunk
                          #:key [reckless? #f])
    (define actor-refr
      (actormap-spawn! actormap
                       (lambda (bcom)
                         (lambda ()
                           (call-with-values thunk list)))))
    (define actormap-poker!
      (if reckless?
          actormap-reckless-poke!
          actormap-poke!))
    (apply values (actormap-poker! actormap actor-refr)))


  
  ;; Vats
  ;; ----

;;;                .=======================.
;;;                |Internal Vat Schematics|
;;;                '======================='
;;;
;;;             stack           heap
;;;              ($)         (actormap)
;;;           .-------.----------------------. -.
;;;           |       |                      |  |
;;;           |       |   .-.                |  |
;;;           |       |  (obj)         .-.   |  |
;;;           |       |   '-'         (obj)  |  |
;;;           |  __   |                '-'   |  |
;;;           | |__>* |          .-.         |  |- actormap
;;;           |  __   |         (obj)        |  |  territory
;;;           | |__>* |          '-'         |  |
;;;           |  __   |                      |  |
;;;           | |__>* |                      |  |
;;;           :-------'----------------------: -'
;;;     queue |  __    __    __              | -.
;;;      (<-) | |__>* |__>* |__>*            |  |- event loop
;;;           '------------------------------' -'  territory
;;;
;;;
;;; Finished reading core.rkt and thought "gosh what I want more out of
;;; life is more ascii art diagrams"?  Well this one is pretty much figure
;;; 14.2 from Mark S. Miller's dissertation (with some Goblins specific
;;; modifications):
;;;   http://www.erights.org/talks/thesis/
;;;
;;; If we just look at the top of the diagram, we can look at the world as
;;; it exists purely in terms of actormaps.  The right side is the actormap
;;; itself, effectively as a "heap" of object references mapped to object
;;; behavior (not unlike how in memory-unsafe languages pointers map into
;;; areas of memory).  The left-hand side is the execution of a
;;; turn-in-progress... the bottom stubby arrow corresponds to the initial
;;; invocation against some actor in the actormap, and stacked on top are
;;; calls to other actors via immediate call-return behavior using $.
;;;
;;; Vats come in when we add the bottom half of the diagram: the event
;;; loop!  An event loop manages a queue of messages that are to be handled
;;; asynchronously... one after another after another.  Each message which
;;; is handled gets pushed onto the upper-left hand stack, executes, and
;;; bottoms out in some result (which the vat then uses to resolve any
;;; promise that is waiting on this message).  During its execution, this
;;; might result in building up more messages by calls sent to <-, which,
;;; if to refrs in the same vat, will be put on the queue (FIFO order), but
;;; if they are in another vat will be sent there using the reference's vat
;;; or machine connector (depending on if local/remote).
;;;
;;; Anyway, you could implement a vat-like event loop yourself, but this
;;; module implements the general behavior.  The most important thing if
;;; you do so is to resolve promises based on turn result and also
;;; implement the vat-connnector behavior (currently the handle-message
;;; and vat-id methods, though it's not unlikely this module will get
;;; out of date... oops)
  )
