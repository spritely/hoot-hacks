;; Stub providing only what Goblins needs of guile-gcrypts (gcrypt pk-crypto).

(define-module (guile-compat gcrypt pk-crypto)
  #:use-module (guile-compat gcrypt random)
  #:use-module (hoot hashtables)
  #:export (sign
            verify
            generate-key
            find-sexp-token
            canonical-sexp->sexp
            sexp->canonical-sexp))

(define sign
  (let ((known-sigs (make-hashtable)))
    (lambda (data secret-key)
      (let ((key (cons data secret-key)))
        (unless (hashtable-contains? known-sigs key)
          (hashtable-set! known-sigs key
                          `(sig-val (eddsa (r ,(gen-random-bv 32))
                                           (s ,(gen-random-bv 32))))))
        (hashtable-ref known-sigs key)))))

(define (verify signature data public-key) #t)

(define (generate-key params)
  `(key-data
    (public-key (ecc (curve Ed25519)
                     (flags eddsa)
                     (q ,(gen-random-bv))))
    (private-key (ecc (curve Ed25519)
                      (flags eddsa)
                      (q ,(gen-random-bv))))))

(define (find-sexp-token sexp token)
  (do ((s (cdr sexp) (cdr s)))
      ((eq? (car (car s)) token) (car s))))

(define (canonical-sexp->sexp sexp)
  sexp)

(define (sexp->canonical-sexp sexp)
  sexp)
