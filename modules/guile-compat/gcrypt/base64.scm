;; -*- mode: scheme; coding: utf-8 -*-
;;
;; This module was renamed from (weinholt text base64 (1 0 20100612)) to
;; (guix base64) by Nikita Karetnikov <nikita@karetnikov.org> on
;; February 12, 2014.  It was later renamed to (gcrypt base64) by
;; Christine Lemmer-Webber <cwebber@dustycloud.org> on May 20, 2017.
;;
;; Some optimizations made by Ludovic Courtès <ludo@gnu.org>, 2015.
;; Turned into a Guile module (instead of R6RS).
;;
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation; either version 3 of
;; the License, or (at your option) any later version.
;;
;; guile-gcrypt is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with guile-gcrypt.  If not, see <http://www.gnu.org/licenses/>.
;;
;; This file incorporates work covered by the following copyright and
;; permission notice:
;;
;;   Copyright © 2009, 2010, 2012, 2013, 2018 Göran Weinholt <goran@weinholt.se>
;;
;;   Permission is hereby granted, free of charge, to any person obtaining a
;;   copy of this software and associated documentation files (the "Software"),
;;   to deal in the Software without restriction, including without limitation
;;   the rights to use, copy, modify, merge, publish, distribute, sublicense,
;;   and/or sell copies of the Software, and to permit persons to whom the
;;   Software is furnished to do so, subject to the following conditions:
;;
;;   The above copyright notice and this permission notice shall be included in
;;   all copies or substantial portions of the Software.
;;
;;   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;;   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;   DEALINGS IN THE SOFTWARE.

;; RFC 4648 Base-N Encodings

;; This file is essentially the upstream file with some procedure locations
;; moved and altered imports for Hoot friendliness.

(define-module (guile-compat gcrypt base64)
  #:export (base64-encode
            base64-decode
            base64-alphabet
            base64url-alphabet
            get-delimited-base64
            put-delimited-base64)
  #:use-module (srfi srfi-11)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 binary-ports)
  #:use-module ((scheme base)
                #:select (read-line)))

;;; Hoot Hacks

(define-syntax-rule (assert true?)
  (unless true?
    (error "Assertion failed" 'true?)))

(define (bit-field n start end)
  (logand (lognot (ash -1 (- end start)))
          (ash n (- start))))

;; This is in (rnrs io ports) but those probably won't work on Hoot so we
;; implement it here.  See (guile-compat ice-9 binary-ports) for
;; open-bytevector-output-port
(define (open-string-output-port)
  (define port (open-output-string))
  (define (extract)
    (get-output-string port))
  (values port extract))

;;; Helpers

;; Create a lookup table for the alphabet and remember the latest table.
(define get-decode-table
  (let ((ascii-table #f)
        (extra-table '())               ;in the unlikely case of unicode chars
        (table-alphabet #f))
    (lambda (alphabet)
      (unless (eq? alphabet table-alphabet)
        ;; Rebuild the table.
        (do ((ascii (make-vector 128 #f))
             (extra '())
             (i 0 (+ i 1)))
            ((= i (string-length alphabet))
             (set! ascii-table ascii)
             (set! extra-table extra))
          (let ((c (char->integer (string-ref alphabet i))))
            (if (fx<=? c 127)
                (vector-set! ascii c i)
                (set! extra (cons (cons c i) extra)))))
        (set! table-alphabet alphabet))
      (values ascii-table extra-table))))

(define-syntax define-alias
  (syntax-rules ()
    ((_ new old)
     (define-syntax new (identifier-syntax old)))))

;; Force the use of Guile's own primitives to avoid the overhead of its 'fx'
;; procedures.

(define-alias fxbit-field bit-field)
(define-alias fxarithmetic-shift ash)
(define-alias fxarithmetic-shift-left ash)
(define-alias fxand logand)
(define-alias fxior logior)
(define-alias fxxor logxor)
(define-alias fx=? =)
(define-alias fx<=? <=)
(define-alias fxzero? zero?)
(define-alias fx+ +)
(define-alias fx- -)
(define-alias fxmod modulo)
(define-alias mod modulo)

;;; Exports

(define base64-alphabet
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")

(define base64url-alphabet
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_")

(define* (base64-encode bv
                        #:optional
                        (start 0)
                        (end (bytevector-length bv))
                        (line-length #f)
                        (no-padding #f)
                        (alphabet base64-alphabet)
                        (port #f))
  "Base64 encode the bytes [start,end[ in the given bytevector.
Lines are limited to line-length characters (unless #f),
which must be a multiple of four. To omit the padding
characters (=) set no-padding to a true value. If port is
#f, returns a string."
  (assert (or (not line-length) (zero? (mod line-length 4))))
  (let-values (((p extract) (if port
                                (values port (lambda () (values)))
                                (open-string-output-port))))
    (letrec ((put (if line-length
                      (let ((chars 0))
                        (lambda (p c)
                          (when (fx=? chars line-length)
                            (set! chars 0)
                            (newline p))
                          (set! chars (fx+ chars 1))
                          (write-char c p)))
                      (lambda (p c)
                        (write-char c p)))))
      (let lp ((i start))
        (cond ((= i end))
              ((<= (+ i 3) end)
               (let ((x (bytevector-uint-ref bv i (endianness big) 3)))
                 (put p (string-ref alphabet (fxbit-field x 18 24)))
                 (put p (string-ref alphabet (fxbit-field x 12 18)))
                 (put p (string-ref alphabet (fxbit-field x 6 12)))
                 (put p (string-ref alphabet (fxbit-field x 0 6)))
                 (lp (+ i 3))))
              ((<= (+ i 2) end)
               (let ((x (fxarithmetic-shift-left (bytevector-u16-ref bv i (endianness big)) 8)))
                 (put p (string-ref alphabet (fxbit-field x 18 24)))
                 (put p (string-ref alphabet (fxbit-field x 12 18)))
                 (put p (string-ref alphabet (fxbit-field x 6 12)))
                 (unless no-padding
                   (put p #\=))))
              (else
               (let ((x (fxarithmetic-shift-left (bytevector-u8-ref bv i) 16)))
                 (put p (string-ref alphabet (fxbit-field x 18 24)))
                 (put p (string-ref alphabet (fxbit-field x 12 18)))
                 (unless no-padding
                   (put p #\=)
                   (put p #\=)))))))
    (extract)))

(define* (base64-decode str
                        #:optional
                        (alphabet base64-alphabet)
                        (port #f)
                        (strict? #t)
                        (strict-padding? #t))
  "Decode a base64 string, optionally ignoring non-alphabet
characters and lack of padding."
  (define (pad? c) (eqv? c (char->integer #\=)))
  (let-values (((p extract) (if port
                                (values port (lambda () (values)))
                                (open-bytevector-output-port)))
               ((ascii extra) (get-decode-table alphabet)))
    (define-syntax lookup
      (syntax-rules ()
        ((_ c) (or (and (fx<=? c 127) (vector-ref ascii c))
                   (cond ((assv c extra) => cdr)
                         (else #f))))))
    (let lp-restart ((str str))
      (let* ((len (if strict?
                      (string-length str)
                      (let lp ((i (fx- (string-length str) 1)))
                        ;; Skip trailing invalid chars.
                        (cond ((fxzero? i) 0)
                              ((let ((c (char->integer (string-ref str i))))
                                 (or (lookup c) (pad? c)))
                               (fx+ i 1))
                              (else (lp (fx- i 1))))))))
        (let lp ((i 0))
          (cond
           ((fx=? i len)
            (extract))
           ((fx<=? i (fx- len 4))
            (let lp* ((c1 (char->integer (string-ref str i)))
                      (c2 (char->integer (string-ref str (fx+ i 1))))
                      (c3 (char->integer (string-ref str (fx+ i 2))))
                      (c4 (char->integer (string-ref str (fx+ i 3))))
                      (i i))
              (let ((i1 (lookup c1)) (i2 (lookup c2))
                    (i3 (lookup c3)) (i4 (lookup c4)))
                (cond
                 ((and i1 i2 i3 i4)
                  ;; All characters present and accounted for.
                  ;; The most common case.
                  (let ((x (fxior (fxarithmetic-shift-left i1 18)
                                  (fxarithmetic-shift-left i2 12)
                                  (fxarithmetic-shift-left i3 6)
                                  i4)))
                    (put-u8 p (fxbit-field x 16 24))
                    (put-u8 p (fxbit-field x 8 16))
                    (put-u8 p (fxbit-field x 0 8))
                    (lp (fx+ i 4))))
                 ((and i1 i2 i3 (pad? c4) (= i (- len 4)))
                  ;; One padding character at the end of the input.
                  (let ((x (fxior (fxarithmetic-shift-left i1 18)
                                  (fxarithmetic-shift-left i2 12)
                                  (fxarithmetic-shift-left i3 6))))
                    (put-u8 p (fxbit-field x 16 24))
                    (put-u8 p (fxbit-field x 8 16))
                    (lp (fx+ i 4))))
                 ((and i1 i2 (pad? c3) (pad? c4) (= i (- len 4)))
                  ;; Two padding characters.
                  (let ((x (fxior (fxarithmetic-shift-left i1 18)
                                  (fxarithmetic-shift-left i2 12))))
                    (put-u8 p (fxbit-field x 16 24))
                    (lp (fx+ i 4))))
                 ((not strict?)
                  ;; Non-alphabet characters.
                  (let lp ((i i) (c* '()) (n 4))
                    (cond ((fxzero? n)
                           ;; Found four valid characters.
                           (lp* (cadddr c*) (caddr c*) (cadr c*) (car c*)
                                (fx- i 4)))
                          ((fx=? i len)
                           (error 'base64-decode
                                  "Invalid input in non-strict mode."
                                  i c*))
                          (else
                           ;; Gather alphabetic (or valid
                           ;; padding) characters.
                           (let ((c (char->integer (string-ref str i))))
                             (cond ((or (lookup c)
                                        (and (pad? c)
                                             (fx<=? n 2)
                                             (fx=? i (fx- len n))))
                                    (lp (fx+ i 1) (cons c c*) (fx- n 1)))
                                   (else
                                    (lp (fx+ i 1) c* n))))))))
                 (else
                  (error 'base64-decode
                         "Invalid input in strict mode."
                         c1 c2 c3 c4))))))
           ((not strict-padding?)
            ;; Append an appropriate amount of padding after the
            ;; remaining characters.
            (if (<= 2 (- len i) 3)
                (lp-restart (string-append (substring str i (string-length str))
                                           (if (= (- len i) 2) "==" "=")))
                (error 'base64-decode "The input is too short." i)))
           (else
            (error 'base64-decode
                   "The input is too short, it may be missing padding."
                   i))))))))

(define* (get-delimited-base64 port #:optional (strict #t))
  "Reads the common -----BEGIN/END type----- delimited format from
the given port. Returns two values: a string with the type and a
bytevector containing the base64 decoded data. The second value
is the eof object if there is an eof before the BEGIN delimiter."
  (define (get-first-data-line port)
    ;; Some MIME data has header fields in the same format as mail
    ;; or http. These are ignored.
    (let ((line (read-line port)))
      (cond ((eof-object? line) line)
            ((string-index line #\:)
             (let lp ()                 ;read until empty line
               (let ((line (read-line port)))
                 (if (string=? line "")
                     (read-line port)
                     (lp)))))
            (else line))))
  (let ((line (read-line port)))
    (cond ((eof-object? line)
           (values "" (the-eof-object)))
          ((string=? line "")
           (get-delimited-base64 port))
          ((and (string-prefix? "-----BEGIN " line)
                (string-suffix? "-----" line))
           (let* ((type (substring line 11 (- (string-length line) 5)))
                  (endline (string-append "-----END " type "-----")))
             (let-values ([(outp extract) (open-bytevector-output-port)])
               (let lp ((previous "") (line (get-first-data-line port)))
                 (cond ((eof-object? line)
                        (error 'get-delimited-base64
                               "unexpected end of file"))
                       ((string-prefix? "-" line)
                        (unless (string=? line endline)
                          (error 'get-delimited-base64
                                 "bad end delimiter" type line))
                        (values type (extract)))
                       ((and (= (string-length line) 5)
                             (string-prefix? "=" line))
                        ;; Skip Radix-64 checksum
                        (lp previous (read-line port)))
                       ((not (fxzero? (fxmod (fx+ (string-length previous)
                                                  (string-length line))
                                             4)))
                        ;; OpenSSH outputs lines with a bad length
                        (lp (string-append previous line)
                            (read-line port)))
                       (else
                        (base64-decode (string-append previous line) base64-alphabet outp)
                        (lp "" (read-line port))))))))
          (else       ;skip garbage (like in openssl x509 -in foo -text output).
           (get-delimited-base64 port)))))

(define* (put-delimited-base64 port type bv #:optional (line-length 76))
  (display (string-append "-----BEGIN " type "-----\n") port)
  (base64-encode bv 0 (bytevector-length bv)
                 line-length #f base64-alphabet port)
  (display (string-append "\n-----END " type "-----\n") port))
