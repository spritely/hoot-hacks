;; Stub of guile-gcrypt's (gcrypt random). Hoot lacks a standard randomness
;; source so this module does not actual do anything random.

(define-module (guile-compat gcrypt random)
  #:use-module (guile-compat gcrypt base64)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:export (%gcry-weak-random
            %gcry-strong-random
            %gcry-very-strong-random
            gen-random-bv
            random-token))

(define %gcry-weak-random 0)  ; not used
(define %gcry-strong-random 1)
(define %gcry-very-strong-random 2)

;; XXX this is fake
;; Because we don't have randomness, but we do need different nonces every time
;; we generate a nonce, we use this hack. Essentially, each new "random"
;; bytevector is filled with the next character up (in ASCII terms) from !, the
;; first printable non-whitespace character.
(define gen-random-bv
  (let ((char 33))
    (lambda* (#:optional (bv-length 50) (level %gcry-strong-random))
      (set! char (modulo (+ char 1) 256))
      (make-bytevector bv-length char))))

(define* (gen-random-nonce #:optional (bv-length 50))
  (gen-random-bv bv-length))

(define* (random-token #:optional (bv-length 50)
                       (type 'strong))
  "Generate a random token.

Generates a token of bytevector BV-LENGTH, default 30.

The default TYPE is 'strong.  Possible values are:
 - strong: Uses libgcrypt's gcry_randomize procedure with level
   GCRY_STRONG_RANDOM (\"use this level for session keys and similar
   purposes\").
 - very-strong: Also uses libgcrypt's gcry_randomize procedure with level
   GCRY_VERY_STRONG_RANDOM (\"Use this level for long term key material\")
 - nonce: Uses libgcrypt's gcry_xcreate_nonce, whose documentation I'll
   just quote inline:

     Fill BUFFER with LENGTH unpredictable bytes.  This is commonly
     called a nonce and may also be used for initialization vectors and
     padding.  This is an extra function nearly independent of the other
     random function for 3 reasons: It better protects the regular
     random generator's internal state, provides better performance and
     does not drain the precious entropy pool."
  (let ((bv (match type
              ('strong
               (gen-random-bv bv-length %gcry-strong-random))
              ('very-strong
               (gen-random-bv bv-length %gcry-very-strong-random))
              ('nonce
               (gen-random-nonce bv-length)))))
    (base64-encode bv 0 bv-length #f #t base64url-alphabet)))
