;; Stub providing only what Goblins needs from guile-gcrypt's (gcrypt hash).

(define-module (guile-compat gcrypt hash)
  #:use-module (guile-compat gcrypt random)
  #:export (sha256))

(define (sha256 text)
  text)
