;;; Wrapper of the JavaScript WebSocket API for Hoot

;;; Copyright © 2024 Juliana Sims <juli@incana.org>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; This library provides Hoot-friendly access to the JavaScript WebSocket API:
;;; https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API

;;; Code:

(define-module (websocket)
  #:use-module (hoot ffi)
  #:use-module (srfi srfi-9)
  #:export (message-event-data

            make-websocket
            websocket?
            websocket-close!
            websocket-on-message
            websocket-on-open
            websocket-send))

(define-foreign add-event-listener!
  "element" "addEventListener"
  (ref null extern) (ref string) (ref null extern) -> none)

(define-foreign message-event-data
  "MessageEvent" "data"
  (ref null extern) -> (ref string))

(define-foreign ws-new
  "WebSocket" "new"
  (ref string) -> (ref extern))

(define-foreign ws-close
  "WebSocket" "close"
  (ref extern) -> none)

(define-foreign ws-send
  "WebSocket" "send"
  (ref null extern) (ref string) -> none)

(define-record-type <websocket>
  (wrap-websocket extern)
  websocket?
  (extern unwrap-websocket))

(define (make-websocket url)
  (wrap-websocket (ws-new url)))

(define (websocket-close! ws)
  (ws-close (unwrap-websocket ws)))

(define (websocket-on-message ws proc)
  (add-event-listener! (unwrap-websocket ws)
                       "message"
                       (procedure->external proc)))

(define (websocket-on-open ws proc)
  (add-event-listener! (unwrap-websocket ws)
                       "open"
                       (procedure->external proc)))

(define (websocket-send ws data)
  (ws-send (unwrap-websocket ws)
           data))
