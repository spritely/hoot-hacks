;;; Copyright 2021-2022 Christine Lemmer-Webber
;;; Copyright 2022 Jessica Tallon
;;; Copyright 2023 David Thompson
;;; Copyright 2023 Juliana Sims
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (goblins vat)
  #:use-module (goblins base-io-ports)
  #:use-module (goblins core)
  #:use-module (goblins core-types)
  #:use-module (goblins inbox)
  ;;#:use-module (goblins abstract-types)
  ;;#:use-module (goblins default-vat-scheduler)
  ;;#:use-module (goblins utils random-name)
  ;;#:use-module (goblins utils ring-buffer)
  #:use-module (fibers)
  #:use-module (fibers conditions)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 control)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 match)
  #:use-module (ice-9 q)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export (&vat-turn-error
            vat-turn-error-event

            vat-envelope?
            vat-envelope-message
            vat-envelope-timestamp
            vat-envelope-return?

            all-vats
            lookup-vat
            make-vat
            vat?
            vat-id
            vat-name
            vat-connector
            vat-running
            ;; vat-clock
            vat-running?
            vat-halt!
            vat-start!
            call-with-vat
            with-vat
            make-fibrous-vat
            spawn-fibrous-vat
            spawn-vat

            ;; call-system-op-with-vat

            syscaller-free-fiber

            ;; define-vat-run

            ;; and here's a hack, but maybe someone wants
            ;; to start with it and tweak it
            port-redirect-dynamic-wrap))

;; Hoot hacks

(define (default-vat-scheduler) #f)

(define (make-vat-log . args) #f)

;; alright y'all, let's make some fake mutices
(define (make-mutex)
  (let ((locked? #f))
    (lambda args
      (match args
        (('state) locked?)
        (('lock)
         (unless locked?
           (set! locked? #t))
         locked?)
        (('unlock)
         (when locked?
           (set! locked? #f))
         locked?)))))

(define (mutex-locked? mut)
  (mut 'state))

(define (lock-mutex mut)
  (mut 'lock))

(define (unlock-mutex mut)
  (mut 'unlock))

(define-syntax-rule (with-mutex m e0 e1 ...)
  (let ((x m))
    (dynamic-wind
      (lambda ()
        ;; XXX this may not work but if it does we can at least test with it
        (if (mutex-locked? x)
            (error "Attempted to lock a locked mutex")
            (lock-mutex x)))
      (lambda () (begin e0 e1 ...))
      (lambda () (unlock-mutex x)))))

;; Vats
;; ----

;;;                .=======================.
;;;                |Internal Vat Schematics|
;;;                '======================='
;;;
;;;             stack           heap
;;;              ($)         (actormap)
;;;           .-------.----------------------. -.
;;;           |       |                      |  |
;;;           |       |   .-.                |  |
;;;           |       |  (obj)         .-.   |  |
;;;           |       |   '-'         (obj)  |  |
;;;           |  __   |                '-'   |  |
;;;           | |__>* |          .-.         |  |- actormap
;;;           |  __   |         (obj)        |  |  territory
;;;           | |__>* |          '-'         |  |
;;;           |  __   |                      |  |
;;;           | |__>* |                      |  |
;;;           :-------'----------------------: -'
;;;     queue |  __    __    __              | -.
;;;      (<-) | |__>* |__>* |__>*            |  |- event loop
;;;           '------------------------------' -'  territory
;;;
;;;
;;; Finished reading core.rkt and thought "gosh what I want more out of
;;; life is more ascii art diagrams"?  Well this one is pretty much figure
;;; 14.2 from Mark S. Miller's dissertation (with some Goblins specific
;;; modifications):
;;;   http://www.erights.org/talks/thesis/
;;;
;;; If we just look at the top of the diagram, we can look at the world as
;;; it exists purely in terms of actormaps.  The right side is the actormap
;;; itself, effectively as a "heap" of object references mapped to object
;;; behavior (not unlike how in memory-unsafe languages pointers map into
;;; areas of memory).  The left-hand side is the execution of a
;;; turn-in-progress... the bottom stubby arrow corresponds to the initial
;;; invocation against some actor in the actormap, and stacked on top are
;;; calls to other actors via immediate call-return behavior using $.
;;;
;;; Vats come in when we add the bottom half of the diagram: the event
;;; loop!  An event loop manages a queue of messages that are to be handled
;;; asynchronously... one after another after another.  Each message which
;;; is handled gets pushed onto the upper-left hand stack, executes, and
;;; bottoms out in some result (which the vat then uses to resolve any
;;; promise that is waiting on this message).  During its execution, this
;;; might result in building up more messages by calls sent to <-, which,
;;; if to refrs in the same vat, will be put on the queue (FIFO order), but
;;; if they are in another vat will be sent there using the reference's vat
;;; or CapTP connector (depending on if local/remote).
;;;
;;; Anyway, you could implement a vat-like event loop yourself, but this
;;; module implements the general behavior.  The most important thing if
;;; you do so is to resolve promises based on turn result and also
;;; implement the vat-connnector behavior (currently the handle-message
;;; and vat-id methods, though it's not unlikely this module will get
;;; out of date... oops)

;; The purpose of this is to prevent issues where a user hacking
;; with Geiser's buffer evaluation commands (eg C-x C-e)
;; launches a vat, and things weirdly break... because geiser
;; sets redirects output so that it can capture it to display to the
;; user when hacking that way, but those ports are closed at the
;; end of the evaulation.  But since the vat would run in its own
;; fiber/thread, any attempts to write to output/error ports would
;; throw an exception.  This redirects them "back".
(define (port-redirect-dynamic-wrap proc)
  (parameterize ((current-output-port %base-output-port)
                 (current-error-port %base-error-port))
    (proc)))

;; TODO <vat-persistence> vat event logging etc. go here

(define-record-type <vat-event>
  (make-vat-event type churn timestamp far-timestamp message snapshot)
  vat-event?
  (type vat-event-type)                 ; either 'send' or 'receive'
  (churn vat-event-churn)
  (timestamp vat-event-timestamp)
  (far-timestamp vat-event-far-timestamp)
  (message vat-event-message)
  (snapshot vat-event-snapshot set-vat-event-snapshot!))

;; Vats
;; ====

(define-exception-type &vat-turn-error &error
  make-vat-turn-error
  vat-turn-error?
  (event vat-turn-error-event))

;; Vat envelopes contain a message, are postmarked with a Lamport
;; timestamp to indicate when it was sent, and have a flag that
;; indicates if the sender wants a reply.  Currently, the return? flag
;; is only used to support returning values to the user via
;; call-with-vat.
(define-record-type <vat-envelope>
  (make-vat-envelope message timestamp return?)
  vat-envelope?
  (message vat-envelope-message)
  (timestamp vat-envelope-timestamp)
  (return? vat-envelope-return?))

(define-record-type <vat>
  (%make-vat id name actormap running connector current-churn
             clock logging? log start-proc halt-proc send-proc
             persistence-env)
  vat?
  (id vat-id)
  (name vat-name)
  (actormap vat-actormap)
  (running vat-running)
  (connector vat-connector)
  (current-churn vat-current-churn set-vat-current-churn!)
  (clock %vat-clock)
  (logging? %vat-logging?)
  (log vat-log)
  (start-proc vat-start-proc)
  (halt-proc vat-halt-proc)
  (send-proc vat-send-proc)
  (persistence-env vat-persistence-env))

;; A global table of vats keyed by id.
(define *vats*
  (let ((table #f))
    (lambda ()
      (unless table
        (set! table (make-hash-table)))
      table)))

(define (all-vats)
  (hash-map->list (lambda (k v) v) (*vats*)))

(define (lookup-vat id)
  (hashv-ref (*vats*) id))

(define register-vat!
  (let ((mutex (make-mutex)))
    (lambda (vat)
      (with-mutex mutex
        (hashv-set! (*vats*) (vat-id vat) vat)))))

;; A global id counter for vats.
(define *vat-id-counter* (make-atomic-box 0))

(define (next-vat-id)
  (let* ((id (atomic-box-ref *vat-id-counter*)))
    ;; If the atomic box was updated in another thread then the id
    ;; we've just generated is no good and the counter will not be
    ;; updated.  Loop until we get a good one.
    (if (eq? (atomic-box-compare-and-swap! *vat-id-counter* id (+ id 1)) id)
        id
        (next-vat-id))))

(define default-log-capacity 256)

(define* (make-vat #:key persistence-env name start halt send log?
                   (log-capacity default-log-capacity))
  "Return a new vat named NAME.  Vat behavior is determined by three
event hooks:

START: A procedure that starts the vat process, presumably in a new
thread or other non-blocking manner. Accepts one argument: a procedure
which takes a message as its one argument and churns the underlying
actormap for the vat.

HALT: A thunk that stops the vat process.

SEND: A procedure that accepts a message to handle within the vat
process and a boolean flag indicating if the message result needs to
be returned to the sender or not.

If LOG? is #t, event logging is enabled.  By default, logging is
disabled.  LOG-CAPACITY events will be retained in the log.

Type: (Optional (#:name (U String Symbol)))
(Optional (#:start (Message -> Void))) (Optional (#:halt (-> Void)))
(Optional (#:send (Message Boolean -> (U Void Any))))
(Optional (#:log? Boolean))
(Optional (#:log-capacity Positive-Number)) -> Void"
  (define (connector . args)
    (match args
      (('name) (vat-name vat))
      (('handle-message timestamp msg)
       ;; TODO: We should indicate to the procedure which calls this that
       ;; the attempt to send the message failed... so, return an 'ok
       ;; or 'failed message here?
       (when (atomic-box-ref running?)
         (vat-send vat (make-vat-envelope msg timestamp #f))))
      ;; Event log queries.
      ;; (('find-event-by-time timestamp)
      ;;  (vat-log-ref-by-time vat timestamp))
      ;; (('find-event-by-message msg)
      ;;  (vat-log-ref-by-message vat msg))
      ;; (('find-previous-event event)
      ;;  (vat-log-ref-previous vat event))
      ;; (('find-next-events event)
      ;;  (vat-log-ref-next vat event))
      ))
  (define am (make-actormap #:vat-connector connector))
  (define id (next-vat-id))
  (define clock (make-atomic-box 0))
  (define running? (make-atomic-box #f))
  (define logging? (make-atomic-box log?))
  (define log (make-vat-log log-capacity))
  ;; (define index (make-hash-table))
  (define vat
    (%make-vat id name am running? connector 0 clock logging? log
               start halt send persistence-env))
  (register-vat! vat)
  vat)

(define (vat-running? vat)
  "Return #t if VAT is currently running, else #f.

Type: Vat -> Boolean"
  (atomic-box-ref (vat-running vat)))

(define (vat-next-churn-id vat)
  (let ((id (+ (vat-current-churn vat) 1)))
    (set-vat-current-churn! vat id)
    id))

(define (vat-halt! vat)
  "Stop processing turns for VAT.

Type: Vat -> Void"
  (atomic-box-set! (vat-running vat) #f)
  ((vat-halt-proc vat)))

(define* (vat-churn vat msg sent-at)
  (define churn-id (vat-next-churn-id vat))
  (define near-q (make-q))
  (define far-q (make-q))
  (define am (vat-actormap vat))
  (define snapshot ;; (copy-whactormap am)
    #f)
  (define new-am (make-transactormap am))
  (define this-vat-connector (actormap-vat-connector am))
  (define (near-msg? msg)
    (define to-refr (message-or-request-to msg))
    (and (local-refr? to-refr)
         (eq? (local-refr-vat-connector to-refr)
              this-vat-connector)))
  (define* (current-snapshot)
    ;; (define snapshot* (copy-whactormap snapshot))
    ;; (define transactormap (transactormap-reparent new-am snapshot*))
    ;; (transactormap-merge! transactormap)
    ;; snapshot*
    #f)
  (define (queue-messages-appropriately! prev-event msgs)
    (match msgs
      (() 'done)
      ((msg next-msgs ...)
       ;; Last message first.
       (queue-messages-appropriately! prev-event next-msgs)
       ;; Create new event and put it in either the near or far queue.
       (let* ((near? (near-msg? msg))
              (event-type (if near? 'receive 'send))
              ;; (timestamp (vat-next-timestamp vat))
              (timestamp 1337)
              ;; For near messages, the snapshot will be set during
              ;; its turn.  Far messages are not processed in the
              ;; current vat, so we use the current snapshot so users
              ;; can inspect the state of the actormap when the far
              ;; message was sent.
              (snapshot (if near? #f (current-snapshot)))
              (q (if near? near-q far-q)))
         (let ((event (make-vat-event event-type churn-id timestamp
                                      #f msg snapshot)))
           ;; (vat-log-append! vat event prev-event)
           (enq! q event))))))
  (define (turn event)
    ;; (set-vat-event-snapshot! event (current-snapshot))
    (define msg (vat-event-message event))
    (define-values (result buffer-am new-msgs)
      (actormap-turn-message new-am msg #:catch-errors? #t))
    (define result*
      (match result
        (#('ok _)
         (transactormap-buffer-merge! buffer-am)
         result)
        (#('fail exception)
         ;; Decorate exception with the vat event context.
         (let ((vat-error (make-exception (make-vat-turn-error event)
                                          exception)))
           ;; (vat-log-error! vat event vat-error)
           `#(fail ,vat-error)))))
    ;; Queue messages after merging 'buffer-am' so we can take a
    ;; snapshot to associate with far message events.
    (queue-messages-appropriately! event new-msgs)
    result*)
  (define (churn)
    (unless (q-empty? near-q)
      (turn (deq! near-q))
      ;; Continue processing the near messages.
      (churn)))
  ;; Take an initial turn.
  (define received-at ;; (vat-next-timestamp vat sent-at)
    1337)
  (define init-event
    (make-vat-event 'receive churn-id received-at sent-at msg snapshot))
  ;; (vat-log-append! vat init-event #f)
  (define result (turn init-event))
  ;; Turn as many additional times as it takes to run this vat to
  ;; quiescence.
  (churn)
  ;; Dispatch far messages.
  (let loop ()
    (unless (q-empty? far-q)
      (let ((event (deq! far-q)))
        (dispatch-message (vat-event-message event)
                          (vat-event-timestamp event))
        (loop))))

  ;; Persist the changes if needed.
  ;; (vat-maybe-persist-changed-objs! vat new-am)

  ;; And now let's return everything...
  (values result new-am))

(define (vat-start! vat)
  "Start processing turns for VAT.

Type: Vat -> Void"
  (define running? (vat-running vat))
  (define actormap (vat-actormap vat))
  (define (maybe-merge returned am)
    (match returned
      [#('ok rval)
       (transactormap-merge! am)]
      [_ #f]))
  (define (call-with-error-handling thunk handler)
    (call/ec
     (lambda (abort)
       (define (handle-error exn)
         (abort (handler exn)))
       (with-exception-handler handle-error thunk))))
  (define (churn-message msg sent-at)
    (define-values (returned new-actormap)
      (vat-churn vat msg sent-at))
    (maybe-merge returned new-actormap)
    returned)
  (define (churn envelope)
    (call-with-error-handling
      (lambda ()
        (let* ((msg-or-proc (vat-envelope-message envelope))
               (sent-at (vat-envelope-timestamp envelope)))
          (match msg-or-proc
            ;; Special case, we're asking to "run" a specific
            ;; procedure for call-with-vat
            ((? procedure? thunk)
             ;; The user provided thunk is going to be called
             ;; asynchronously within a vat turn, likely in another thread,
             ;; which makes handling multiple return values tricky.  To
             ;; make things easy for vat implementations, we wrap up all of
             ;; the original thunk's return values into a list so there's
             ;; only a single value to pass back.  Here in the caller's
             ;; thread, the list gets converted back into multiple return
             ;; values.
             (define (multi-value-thunk)
               (call-with-values thunk list))
             ;; Spawn a throwaway actor whose behavior is just to apply the
             ;; thunk.
             (define refr (actormap-spawn! actormap ^call-with-vat
                                           multi-value-thunk))
             (define msg
               (make-message (vat-connector vat) refr #f '()))
             (churn-message msg sent-at))
            ;; A "system" operation is a special kind which permits operating on
            ;; the vat's low-level actormap and possibly other features directly.
            ;; ((? vat-system-operation? system-op)
            ;;  (define system-op-proc
            ;;    (vat-system-operation-proc system-op))
            ;;  (system-op-proc vat))
            ;; The usual case.
            (msg
             (churn-message msg sent-at)))))
      (lambda (exn)
        `#(fail ,exn))))
  (unless (atomic-box-ref running?)
    (atomic-box-set! running? #t)
    ((vat-start-proc vat) churn)))

(define (vat-send vat envelope)
  ((vat-send-proc vat) envelope))

;; This simple actor constructor is used to give a descriptive name to
;; the one-off actors created by call-with-vat so that they are
;; clearly marked when debugging.
(define (^call-with-vat _bcom thunk)
  thunk)

(define (call-with-vat vat thunk)
  "Run THUNK in the context of VAT and return the resulting values.

Type: Vat (-> Any) -> Any"
  (if (vat-running? vat)
      (match (vat-send vat (make-vat-envelope thunk 0 #t))
        (#('ok '*awaited*) '*awaited*)
        (#('ok vals) (apply values vals))
        (#('fail err) (raise-exception err)))
      (error "vat is not running" vat)))

(define-syntax-rule (with-vat vat body ...)
  ;;; Evaluate BODY in the context of VAT and return resulting values.
  ;;;
  ;;; Type: Vat Expression ... -> Any
  (call-with-vat vat (lambda () body ...)))

(define* (make-fibrous-vat #:key name log?
                           (persistence-env #f)
                           (log-capacity default-log-capacity)
                           (scheduler (default-vat-scheduler))
                           (dynamic-wrap port-redirect-dynamic-wrap))
  (define done? (make-condition))
  (define-values (enq-ch deq-ch stop?)
    (spawn-delivery-agent))
  (define (start churn)
    (define (handle-message args)
      (match args
        ((envelope return-ch)
         (define result (churn envelope))
         ;; We have the put-message be run in its own fiber so that if
         ;; the other side isn't listening for it anymore, the vat
         ;; itself doesn't end up blocked.
         (syscaller-free-fiber
          (lambda ()
            (put-message return-ch result))))
        (envelope
         (churn envelope))))
    (define (loop)
      ;; This loop will repeatedly handle a new message or detect if
      ;; the 'done?' condition has been signalled.  The message
      ;; handler will churn the vat and loop.  The loop terminates
      ;; when the 'done?' condition is signalled.
      (and (perform-operation
            (choice-operation (wrap-operation (get-operation deq-ch)
                                              (lambda (args)
                                                (handle-message args)
                                                #t))
                              (wrap-operation (wait-operation done?)
                                              (lambda () #f))))
           (loop)))
    (dynamic-wrap
     (lambda ()
       (syscaller-free
        (lambda ()
          (spawn-fiber loop))))))
  (define (halt)
    (signal-condition! done?)
    *unspecified*)
  (define (send envelope)
    (if (vat-envelope-return? envelope)
        (let ((return-ch (make-channel)))
          (put-message enq-ch (list envelope return-ch))
          (get-message return-ch))
        (put-message enq-ch envelope)))
  (make-vat #:name name
            #:log? log?
            #:log-capacity log-capacity
            #:start start
            #:halt halt
            #:send send
            #:persistence-env persistence-env))

(define* (spawn-fibrous-vat #:key name log?
                            (persistence-env #f)
                            (log-capacity default-log-capacity)
                            (scheduler (default-vat-scheduler))
                            (dynamic-wrap port-redirect-dynamic-wrap))
  (let ((vat (make-fibrous-vat #:name name
                               #:log? log?
                               #:log-capacity log-capacity
                               #:scheduler scheduler
                               #:dynamic-wrap dynamic-wrap
                               #:persistence-env persistence-env)))
    (vat-start! vat)
    vat))


(define* (spawn-vat #:key name log? (log-capacity default-log-capacity))
  "Create and return a reference to a new vat. If provided, NAME is
the debug name of the vat. If LOG? is #t, log vat events, otherwise
do not. If provided, LOG-CAPACITY is the number of events to retain in
the log.

Type: (Optional (#:name (U String Symbol)) (Optional (#:log? Boolean))
(Optional (#:log-capacity Positive-Number)) -> Vat"
  (spawn-fibrous-vat #:name name
                     #:log? log?
                     #:log-capacity log-capacity))

(define (syscaller-free-fiber thunk)
  (syscaller-free
   (lambda ()
     (spawn-fiber thunk))))

;; TODO spawn-fibrous-vow fibrous vat persistence go here
