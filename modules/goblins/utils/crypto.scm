;;; Copyright 2021 Christine Lemmer-Webber
;;; Copyright 2022 Jessica Tallon
;;; Copyright © 2024 Juliana Sims <juli@incana.org>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (goblins utils crypto)
  #:use-module (guile-compat gcrypt base64)
  #:use-module (hoot ffi)
  #:use-module (ice-9 match)
  #:use-module (fibers promises)
  #:use-module ((rnrs bytevectors)
                #:hide (bytevector-copy))
  #:use-module ((scheme base)
                #:select (bytevector-append
                          bytevector-copy))
  #:use-module ((srfi srfi-1)
                #:select (fold))
  #:export (sha256
            sha256d
            strong-random-bytes
            url-base64-encode
            url-base64-decode
            generate-key-pair
            gen-random-bv
            key-pair->public-key
            key-pair->private-key
            sign
            verify
            signature-sexp?
            captp-public-key->crypto-public-key
            captp-signature->crypto-signature))

;;; JS Types

(define (bytevector->uint8array bv)
  "Convert @var{bv} to a Uint8Array

Type: Bytevector -> Uint8Array"
  (convert-arrays bv bytevector-length make-uint8array
                  uint8array-set! bytevector-u8-ref))

(define (uint8array->bytevector u8a)
  "Convert @var{u8a} to a Bytevector

Type: Uint8Array -> Bytevector"
  (convert-arrays u8a uint8array-length make-bytevector
                  bytevector-u8-set! uint8array-ref))

;; Helper macro for conversion because DRY
(define-syntax-rule (convert-arrays val len-proc make-proc set-proc! ref-proc)
  (let* ((len (len-proc val))
         (new-val (make-proc len)))
    (do ((i 0 (1+ i)))
        ((>= i len) new-val)
      (set-proc! new-val i (ref-proc val i)))))

;; length -> Uint8Array
(define-foreign make-uint8array
  "typedArray" "makeUint8Array" i32 -> (ref extern))
;; Uint8Array -> length
(define-foreign uint8array-length
  "typedArray" "Uint8ArrayLength" (ref extern) -> i32)
;; Uint8Array, idx -> byte
(define-foreign uint8array-ref
  "typedArray" "Uint8ArrayRef" (ref extern) i32 -> i32)
;; Uint8Array, idx, byte -> ()
(define-foreign uint8array-set!
  "typedArray" "Uint8ArraySet" (ref extern) i32 i32 -> none)

;;; Hashing

(define (sha256d input)
  "Hash @var{input} twice using SHA-256

Type: Bytevector -> Bytevector"
  (uint8array->bytevector
   (hash "SHA-256" (hash "SHA-256" input))))

(define (sha256 input)
  (uint8array->bytevector
   (hash "SHA-256" input)))

(define (hash algorithm input)
  "Hash @var{input} using @var{algorithm}

Type: String Uint8Array -> Uint8Array"
  (await (digest algorithm
                 (if (bytevector? input)
                     (bytevector->uint8array input)
                     input))))

;; String TypedArray -> (Promise Uint8Array)
(define-foreign digest
  "crypto" "digest"
  (ref string) (ref extern) -> (ref extern))

;;; Nonce Generation

(define* (strong-random-bytes #:optional (byte-size 32))
  "Generate a Bytevector of @var{byte-size} with strong random values

Type: (Optional UnsignedInteger) -> Bytevector"
  (gen-random-bv byte-size))

(define* (gen-random-bv #:optional (bv-length 50))
  "Generate a random Bytevector of @var{bv-length}

Type: (Optional UnsignedInteger) -> Bytevector"
  (uint8array->bytevector (get-random-values! (make-uint8array bv-length))))

;; Uint8Array -> Uint8Array
(define-foreign get-random-values!
  "crypto" "getRandomValues"
  (ref extern) -> (ref extern))

;;; Base64 Conversion

(define (url-base64-encode bv)
  "Encode @var{bv} to its URL-friendly base64 string format

Type: Bytevector -> String"
  (base64-encode bv 0 (bytevector-length bv) #f #t base64url-alphabet))

(define (url-base64-decode encoded-b64)
  "Decode the URL-friendly base64 string @var{encoded-b64} to binary data

Type: String -> Bytevector"
  (base64-decode encoded-b64 base64url-alphabet #f #t #t))

;; Keys, Signing, and Verification

(define (generate-key-pair)
  "Generate an Ed25519 key pair

Type: -> CryptoKeyPair"
  (await (generate-ed25519-key-pair)))

;; -> (Promise CryptoKeyPair)
(define-foreign generate-ed25519-key-pair
  "crypto" "generateEd25519KeyPair"
  -> (ref extern))

(define (key-pair->private-key key-pair)
  "Return the private key of the Ed25519 @var{key-pair}

Type: CryptoKeyPair -> CryptoKey"
  (extract-private-key key-pair))

;; CryptoKeyPair -> CryptoKey
(define-foreign extract-private-key
  "crypto" "keyPairPrivateKey"
  (ref extern) -> (ref extern))

(define (key-pair->public-key key-pair)
  "Return the public key of the Ed25519 @var{key-pair}

Type: CryptoKeyPair -> S-Expression"
  `(public-key (ecc (curve Ed25519)
                    (flags eddsa)
                    (q ,(export-key
                         (extract-public-key key-pair))))))

;; CryptoKeyPair -> CryptoKey
(define-foreign extract-public-key
  "crypto" "keyPairPublicKey"
  (ref extern) -> (ref extern))

(define (export-key key)
  "Export @var{key} to its raw binary format

Type: CryptoKey -> ByteVector"
  (uint8array->bytevector (await (%export-key key))))

;; CryptoKey -> (Promise Uint8Array)
(define-foreign %export-key
  "crypto" "exportKey"
  (ref extern) -> (ref extern))

(define (captp-public-key->crypto-public-key key)
  "Convert @var{key} from its CapTP wire format to its internal format

Type: S-Expression -> CryptoKey"
  (match key
    (`(public-key (ecc (curve Ed25519)
                       (flags eddsa)
                       (q ,data)))
     (await (import-public-key (bytevector->uint8array data))))))

;; Uint8Array -> (Promise CryptoKey)
(define-foreign import-public-key
  "crypto" "importPublicKey"
  (ref extern) -> (ref extern))

(define (sign data private-key)
  "Sign @var{data} using Ed25519 @var{private-key}

Type: Bytevector CryptoKey -> List"
  (let ((sig-bv (uint8array->bytevector
                 (await (sign-ed25519 (bytevector->uint8array data) private-key)))))
    `(sig-val (eddsa (r ,(bytevector-copy sig-bv 0 32))
                     (s ,(bytevector-copy sig-bv 32))))))

;; Uint8Array CryptoKey -> (Promise Uint8Array)
(define-foreign sign-ed25519
  "crypto" "signEd25519"
  (ref extern) (ref extern) -> (ref extern))

(define (captp-signature->crypto-signature signature)
  "Convert @var{signature} from its CapTP wire format to its internal format

Type: List -> Uint8Array"
  (match signature
    (`(sig-val (eddsa (r ,r) (s ,s)))
     (bytevector->uint8array (bytevector-append r s)))))

(define (verify signature data public-key)
  "Verify @var{signature} of @var{data} using Ed25519 @var{public-key}

Type: Uint8Array Bytevector CryptoKey -> Boolean"
  (await (verify-ed25519 signature
                         (bytevector->uint8array data)
                         public-key)))

;; Uint8Array Uint8Array CryptoKey -> (Promise Boolean)
(define-foreign verify-ed25519
  "crypto" "verifyEd25519"
  (ref extern) (ref extern) (ref extern) -> (ref extern))

(define (signature-sexp? maybe-signature)
  (match maybe-signature
    (`(sig-val (eddsa (r ,(? bytevector?))
                      (s ,(? bytevector?))))
     #t)
    (_ #f)))
