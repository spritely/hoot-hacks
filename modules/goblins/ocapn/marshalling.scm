;; Copyright (C) 2022 Jessica Tallon

;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.
(define-module (goblins ocapn marshalling)
  #:use-module (goblins abstract-types)
  #:use-module (goblins contrib syrup)
  #:export (make-marshallers))

;; This produces syrup serialisation and deserialisation for a record
;; type with the option of a custom syrup record label. This will
;; return two values:
;; 1) marshallers: cons cell (<predicate?> <serialiser>)
;; 2) unmarshaller: cons cell (<predicate?> <unserialiser>)
(define (make-marshallers name predicate serialize deserialize)

  (define marshaller
    (cons
     (lambda (obj)
       (predicate obj))
     (lambda (obj)
       (apply make-tagged* name (serialize obj)))))

  (define unmarshaller
    (cons
     (lambda (label)
       (eq? label name))
     deserialize))

  (values marshaller
          unmarshaller))

;; ;; XXX: this doesn't work but if we make the above change, we want this
;; (define-syntax define-marshallable-record-type
;;   (lambda (stx)
;;     (define (make-serializer-name name)
;;       (datum->syntax name
;;                      (symbol-append 'serialize-
;;                                     (syntax->datum name))))
;;     (syntax-case stx (___)
;;       ((_ name pred (field-name field-accessor ___) ...)
;;        (identifier? #'name)
;;        (with-syntax ((serializer-name (make-serializer-name #'name)))
;;          #`(begin
;;              (define-record-type name
;;                pred
;;                (field-name field-accessor field-getter ___)
;;                ...)

;; ;; (define (serialize-foo foo) (match foo (($ <foo> bar baz) (list bar baz))))
;;              (define (serializer-name record)
;;                (match record
;;                  (($ name field-name ...)
;;                   (list field-name ...))))))))))
