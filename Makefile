demo: websocket-echo-client.wasm

websocket-echo-client.wasm:
	guild compile-wasm -L modules/websocket -L ../guile/module \
		demo/websocket/websocket-echo-client.scm \
		-o demo/websocket/websocket-echo-client.wasm

serve: demo
	@echo "The websocket demo requires you to run the echo server separately."
	guile -e '((@ (hoot web-server) serve) #:work-dir "demo/websocket")'

.PHONY: clean
clean:
	-rm demo/websocket/*.wasm
